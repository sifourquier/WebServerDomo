#include "meteo.h"
#include "httprequesthandler.h"

METEO::METEO()
{
}

QString getLastExecutedQuery(const QSqlQuery& query)
{
	QString sql = query.executedQuery();
	int nbBindValues = query.boundValues().size();

	for(int i = 0, j = 0; j < nbBindValues;)
	{
		int s = sql.indexOf(QLatin1Char('\''), i);
		i = sql.indexOf(QLatin1Char('?'), i);
		if (i < 1)
		{
			break;
		}

		if(s < i && s > 0)
		{
			i = sql.indexOf(QLatin1Char('\''), s + 1) + 1;
			if(i < 2)
			{
				break;
			}
		}
		else
		{
			const QVariant &var = query.boundValue(j);
			QSqlField field(QLatin1String(""), var.type());
			if (var.isNull())
			{
				field.clear();
			}
			else
			{
				field.setValue(var);
			}
			QString formatV = query.driver()->formatValue(field);
			sql.replace(i, 1, formatV);
			i += formatV.length();
			++j;
		}
	}

	return sql;
}


void METEO::service(QString path, HttpRequest& request, HttpResponse &response,  QSqlDatabase* db)
{
	this->db=db;
	 if(path=="currenttemp")
		current(request,response);
	 else if(path=="json/current")
		currentJson(request,response);
    else if(path=="json/graphe")
        grapheJson(request,response);
	 else if(path=="json/stations")
		  stationsJson(request,response);
	else if(path.startsWith("form/"))
	{
		path.remove(0,5);
		response.setHeader("Content-Type", "text/html; charset=utf-8");

		response.write("<html>");
		QSqlQuery searchQuery(*db);
		searchQuery.prepare("SELECT name FROM Stations ORDER BY name");
		searchQuery.exec();
        //if(path=="json")
				response.write(QString("<form action=\"/"+path+"\">").toUtf8());
        //else
            //response.write("<form action=\"/meteo/current\">");
		while(searchQuery.next())
		{
			QString station=searchQuery.value(0).toString();;
			response.write("<input type=\"checkbox\" name=\"Station[]\" value=\"");
			if(request.getParameter("html").toInt())
				response.write("'");
			response.write(station.toUtf8());
			if(request.getParameter("html").toInt())
				response.write("'");
			response.write("\">&nbsp;");
			response.write(station.toUtf8());
			response.write("<br>");
		}
		response.write("<input type=\"submit\"> </form>");
		response.write("</html>",true);
	}
}

void METEO::stationsJson(HttpRequest& request, HttpResponse &response)
{
	QSqlQuery searchQuery(*db);
	QJsonArray jsonarray_stations;
	searchQuery.prepare("SELECT name FROM Stations ORDER BY name");
	searchQuery.exec();
	while(searchQuery.next())
	{
		QJsonObject json_station;
		json_station["name"]=searchQuery.value(0).toString();
		QString station=searchQuery.value(0).toString();
		jsonarray_stations.append(json_station);
	}
	QJsonDocument saveDoc(jsonarray_stations);
	response.write(saveDoc.toJson());
}


void METEO::grapheJson(HttpRequest& request, HttpResponse &response)
{
    response.setHeader("Content-Type", "text; charset=utf-8");

	 QSqlQuery searchQuery(*db);
    QList<QByteArray> stations = request.getParameters("Station[]");
    QJsonArray jsonarray_stations;
    for(int l=0;l<stations.length();l++)
    {
		 QJsonArray jsonarray_types;
        QJsonObject json_station;
        QString name=QString::fromUtf8(stations.at(l));
        json_station["name"]=name;
        for(unsigned int l2=0;l2<sizeof(tab_type_sensor)/sizeof(type_sensor);l2++)
        {
			  QJsonArray jsonarray_values;
							  //"yyyy-MM-dd HH:mm:ss"
            searchQuery.prepare(QString("SELECT * FROM ")+tab_type_sensor[l2].TableName+" WHERE name=:name AND Time BETWEEN :from AND :to ORDER BY Time;   ");
            searchQuery.bindValue(":name", name.toLatin1());
				QDateTime QdtFrom=QDateTime::fromTime_t(QString(request.getParameter("from")).toInt());
				searchQuery.bindValue(":from", QdtFrom.toString("yyyy-MM-dd HH:mm:ss"));
				QDateTime QdtTo=QDateTime::fromTime_t(QString(request.getParameter("to")).toInt());
				searchQuery.bindValue(":to", QdtTo.toString("yyyy-MM-dd HH:mm:ss"));

            searchQuery.exec();

				//response.write(getLastExecutedQuery(searchQuery).toUtf8());
				//response.write(searchQuery.lastError().text().toUtf8());
				//response.write("\n");
				int nbValue=QString(request.getParameter("size")).toInt();
				if(nbValue==0)
					nbValue=360;
				int jump=searchQuery.size()/(nbValue-1);

            while (searchQuery.next()) {
                int val=searchQuery.value(3).toInt();
					 int time=searchQuery.value(2).toDateTime().toTime_t();
                QJsonObject json;
                json["val"]=val;
                json["time"]=time;
                jsonarray_values.append(json);
					 for(int l=1;l<jump;l++)
						 searchQuery.next();
            }
				if(jsonarray_values.size())
				{
					QJsonObject json_type;
					json_type["type"]=tab_type_sensor[l2].Name;
					json_type["unity"]=tab_type_sensor[l2].Unite;
					json_type["mersures"]=jsonarray_values;

					jsonarray_types.append(json_type);
				}
        }
		  json_station["types"]=jsonarray_types;
        jsonarray_stations.append(json_station);
    }
	 QJsonDocument saveDoc(jsonarray_stations);
	 response.write(saveDoc.toJson());
}

void METEO::currentJson(HttpRequest& request, HttpResponse &response)
{
	response.setHeader("Content-Type", "text; charset=utf-8");

	QSqlQuery searchQuery(*db);
	QList<QByteArray> stations = request.getParameters("Station[]");
	QJsonArray jsonarray;
	for(int l=0;l<stations.length();l++)
	{
		QJsonObject json;
		QString name=QString::fromUtf8(stations.at(l));
		json["name"]=name;
		for(unsigned int l2=0;l2<sizeof(tab_type_sensor)/sizeof(type_sensor);l2++)
		{
			searchQuery.prepare(QString("SELECT * FROM ")+tab_type_sensor[l2].TableName+" WHERE name=:name ORDER BY ID DESC LIMIT 1;   ");
			searchQuery.bindValue(":name", name.toLatin1());
			searchQuery.exec();

			//response.write(getLastExecutedQuery(searchQuery).toUtf8());
			//response.write(searchQuery.lastError().text().toUtf8());
			if (searchQuery.next()) {
				int val=searchQuery.value(3).toInt();
				json[tab_type_sensor[l2].Name]=val;

				if(tab_type_sensor[l2].TableName=="BATTERY")
					if(val/((int)(val/1700)+1)<MIN_BAT)
						json["comment"]="low battery";
			}
		}
		jsonarray.append(json);
	}
	QJsonDocument saveDoc(jsonarray);
	response.write(saveDoc.toJson());
}


void METEO::current(HttpRequest& request, HttpResponse &response)
{
	QSqlQuery searchQuery(*db);
	QList<QByteArray> stations = request.getParameters("Station[]");
	response.setHeader("Content-Type", "text/html; charset=utf-8");

	response.write("<html>");

	for(int l=0;l<stations.length();l++)
	{
		response.write("<font size=\"6\">");
		response.write(stations.at(l));
		response.write("</font> ");
		for(unsigned int l2=0;l2<sizeof(tab_type_sensor)/sizeof(type_sensor);l2++)
		{
			searchQuery.prepare(QString("SELECT * FROM ")+tab_type_sensor[l2].TableName+" WHERE name=:name ORDER BY ID DESC LIMIT 1;   ");
			searchQuery.bindValue(":name", QString::fromUtf8(stations.at(l)).toLatin1());
			searchQuery.exec();
			//response.write(getLastExecutedQuery(searchQuery).toUtf8());
			//response.write(searchQuery.lastError().text().toUtf8());
			if (searchQuery.next()) {
				QDateTime date_data=searchQuery.value(2).toDateTime();
				int sec=date_data.secsTo(QDateTime::currentDateTime());
				if(sec<60*10)// 10 minute
				{
					int val=searchQuery.value(3).toInt();
					response.write("<font size=\"");
					response.write(QString::number(tab_type_sensor[l2].Size).toUtf8());
					response.write("\">");
					response.write(QString::number((float)val/tab_type_sensor[l2].Div,'f',tab_type_sensor[l2].PRESISION).toUtf8());
					response.write(tab_type_sensor[l2].Unite);
					response.write("</font>");
					response.write("<br>");
				}
			}
			else
			{
				//response.write("?");
			}

		}
	}
	response.write("</html>",true);

}

