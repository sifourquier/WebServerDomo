#ifndef MYSQL_H
#define MYSQL_H
#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlField>
#include <QtSql/QSqlError>
#include <QtSql/QSqlDriver>
#include <QObject>
#include <QString>
#include <QDebug>
#include <QSettings>


class MYSQL :public QObject
{
    Q_OBJECT;
public:
    MYSQL(QSettings* settings);
	 QSqlDatabase db;

};

#endif // MYSQL_H
