#include "mysql.h"

MYSQL::MYSQL(QSettings* settings)
{
    db = QSqlDatabase::addDatabase("QMYSQL");
    QString host=settings->value("host").toString();
    db.setHostName(host);
    QString user=settings->value("user").toString();
    db.setUserName(user);
    QString pass=settings->value("pass").toString();
    db.setPassword(pass);
    db.setConnectOptions("MYSQL_OPT_RECONNECT=1;CLIENT_INTERACTIVE=1;");
    db.open();
    QSqlQuery query(db);
    query.exec("CREATE DATABASE Meteo;");
    db.close();
    db.setDatabaseName("Meteo");
    if(db.open())
    {
         qDebug()<<"Vous etes maintenant connecte a  " << db.hostName();
    }
    else
    {
        QString erreur=db.lastError().text();
        qDebug(erreur.toUtf8());
        qFatal("La connexion a echouee");
    }
}
