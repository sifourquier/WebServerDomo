/**
  @file
  @author Stefan Frings
*/

#include "requesthandler.h"
#include "filelogger.h"

/** Logger class */
extern FileLogger* logger;

RequestHandler::RequestHandler(QObject* parent, QString configFileName)
	:HttpRequestHandler(parent)
{
	this->configFileName=configFileName;
	MYSQLSettings=new QSettings(configFileName,QSettings::IniFormat);
	MYSQLSettings->beginGroup("MYSQL");
	Mysql = new MYSQL(MYSQLSettings);
}

void RequestHandler::service(HttpRequest& request, HttpResponse& response)
{
	static int l=0;
	l++;
	QByteArray path=request.getPath();
	if(path.startsWith("/"))
		path.remove(0,1);
	qDebug("Conroller: path=%s",path.data());

	// Set a response header


	if(path.startsWith("meteo/") || path=="meteo")
	{
		QSqlDatabase db=QSqlDatabase::cloneDatabase(Mysql->db,QString::number(l));
		if(!db.isValid())
		{
			response.setHeader("Content-Type", "text/html; charset=utf-8");
			response.write("<html>");
			// Return a simple HTML document
			response.write("erreur copy db");
			response.write("</html>",true);
			return;
		}
		db.open();
		if(!db.isOpen())
		{
			response.setHeader("Content-Type", "text/html; charset=utf-8");
			response.write("<html>");
			// Return a simple HTML document
			response.write("erreur open MYSQL");
			response.write("</html>",true);
			return;
		}
		Meteo.service(path.remove(0,6),request, response, &db);
		db.close();

	}
	else if(path.startsWith("html/") || path=="meteo")
	{
		Html.service(path.remove(0,5),request, response);
	}
	else
	{
		response.setHeader("Content-Type", "text/html; charset=utf-8");

		response.write("<html>");
		// Return a simple HTML document
		response.write("<a href=\"/meteo/form/meteo/currenttemp\">Meteo</a></br>");
		response.write("<a href=\"/meteo/form/meteo/json/graphe\">jsongraphe</a></br>");
		response.write("<a href=\"/meteo/form/meteo/json/current\">jsoncurrent</a></br>");
		response.write("<a href=\"/meteo/json/stations\">jsonlistStation</a></br>");
		response.write("<a href=\"/meteo/form/html/current.html?html=1\">current</a></br>");
		response.write("</html>",true);
	}

	qDebug("Conroller: finished request");
	// Clear the log buffer
	if (logger)
	{
		logger->clear();
	}

}
