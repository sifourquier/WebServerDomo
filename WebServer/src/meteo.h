#ifndef METEO_H
#define METEO_H
#include "mysql.h"
#include <QObject>
#include "httprequesthandler.h"
#include <QDateTime>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

#define MIN_BAT 1220

struct type_sensor
{
	 const char* TableName;
	 const char* Name;
    const char* Unite;
    int Size;
    int Div;
    int PRESISION;
};

const type_sensor tab_type_sensor[]={
	 {"TEMPERATURE","temp","°C",7,100,1},
	 {"TEMPERATURE_MIN","tempmin","°C",4,100,1},
	 {"TEMPERATURE_MAX","tempmax","°C",4,100,1},
    /*{"CURRENT","A",4,10,2},
    {"POWER","W",4,10,2},*/
	 {"HUMIDITY","hrel","%",4,100,0},
    /*{"LIGHT","Lum",4,10,2},
    {"VOLTAGE","V",4,10,2},*/
	 {"BATTERY","battery","v",4,1000,2},
	 {"PRESSURE","pressure","hPa",4,100,0},
	 {"SPEED","wingspeed","m/s",4,100,1},
	 {"DIRECTION","wingdir","°",4,100,2},
    //{"ICON","",4}
};

class METEO :public QObject
{

    Q_OBJECT;
public:
    METEO();
	 void service(QString path, HttpRequest& request, HttpResponse& response, QSqlDatabase* db);
    void current(HttpRequest& request, HttpResponse &response);
	 void currentJson(HttpRequest& request, HttpResponse &response);
     void grapheJson(HttpRequest& request, HttpResponse &response);
	  void stationsJson(HttpRequest& request, HttpResponse &response);


private:
	  QSqlDatabase *db;

};

#endif // METEO_H
