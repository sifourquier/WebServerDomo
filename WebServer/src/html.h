#ifndef HTML_H
#define HTML_H
#include "QFile"
#include "httprequesthandler.h"

class HTML : public QObject
{
	Q_OBJECT
public:
	explicit HTML(QObject *parent = 0);
	void service(QString path, HttpRequest& request, HttpResponse& response);

signals:

public slots:
};

#endif // HTML_H
