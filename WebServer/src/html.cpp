#include "html.h"

HTML::HTML(QObject *parent) : QObject(parent)
{

}

void HTML::service(QString path, HttpRequest& request, HttpResponse &response)
{
	response.setHeader("Content-Type", "text/html; charset=utf-8");
	path.remove("../");
	QFile file("/etc/webserver/"+path);
	file.open(QIODevice::ReadOnly);
	QByteArray code=file.readAll();
	QMultiMap<QByteArray,QByteArray>ParameterMap=request.getParameterMap();
	QList<QByteArray> keys=ParameterMap.keys();
	for(int l=0;l<keys.size();l++)
	{
		QList<QByteArray> listparametres=request.getParameters(keys.at(l));
		QByteArray parametres;
		for(l=0;l<listparametres.size()-1;l++)
		{
			parametres.append(listparametres.at(l));
			parametres.append(",");
		}
		parametres.append(listparametres.at(l));
		code.replace("_("+keys.at(l)+")",parametres);
	}
	response.write(code);
}
