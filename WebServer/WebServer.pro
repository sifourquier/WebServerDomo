# This project demonstrates how to use QtWebAppLib by including the
# sources into this project.

TARGET = Web_Server
TEMPLATE = app
QT = core network sql
CONFIG += console

LIBS +=-lmosquitto

HEADERS += \
           src/startup.h \
           src/requesthandler.h \
    src/mysql.h \
    src/meteo.h \
    src/html.h \
    ../QtWebApp/templateengine/template.h \
    ../QtWebApp/templateengine/templatecache.h \
    ../QtWebApp/templateengine/templateglobal.h \
    ../QtWebApp/templateengine/templateloader.h \
    src/documentcache.h \
    src/static.h

SOURCES += src/main.cpp \
           src/startup.cpp \
           src/requesthandler.cpp \
    src/mysql.cpp \
    src/meteo.cpp \
    src/html.cpp \
    ../QtWebApp/templateengine/template.cpp \
    ../QtWebApp/templateengine/templatecache.cpp \
    ../QtWebApp/templateengine/templateloader.cpp \
    src/static.cpp

OTHER_FILES += etc/* logs/* ../readme.txt

#---------------------------------------------------------------------------------------
# The following lines include the sources of the QtWebAppLib library
#---------------------------------------------------------------------------------------

include(../QtWebApp/qtservice/qtservice.pri)
include(../QtWebApp/httpserver/httpserver.pri)
include(../QtWebApp/logging/logging.pri)
# Not used: include(../QtWebApp/templateengine/templateengine.pri)

DISTFILES += \
    etc/webserver/current.html
